# Use Case 01: model performance evaluation
## Description
We've build an image classifier that classifies 4 vehicle types you can see on the road. 


In the data file dt-usecase-01-20181215.csv, you find the predictions from a test set on which we wish to evaluate the image classifier. The test set contains frames from movies that were captured on single vehicles - so each movie only contains frames from a single type of vehicle. 

- p_car
    * probability that image is a car
- p_bike
    * probability that image is a bike
- p_bus
    * probability that image is a bus
- p_motor
    * probability that image is a motor
- image
    * filename of the image. Each image is an image from a movie. e.g. frame_car_a3_182.jpg is frame 182 from movie car_a3.
- label
    * ground truth of the image (bus/car/bike/motor)

## Explore

Explore the dataset visually, and report your findings. 

Some tips:

- Can you explore the class probabilities visually?
- Can you evaluate model performance on the given test set?
- Can you evaluate whether the model performs equally well on all movies?
- Is the test set balanced? Did you use the right performance metrics?